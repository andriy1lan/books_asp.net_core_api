using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using BooksApi.Controllers;
using BooksApi.Models;
using BooksApi.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Moq;
using NUnit.Framework;
using Newtonsoft.Json;

namespace BooksApi.Tests
{

  [TestFixture]
  public class MockingRepositoryTest {

      Mock<IRepository<Book>> mockrepo;
      BooksController controller;

        [OneTimeSetUp]
        public void Setup()
        {
            mockrepo = new Mock<IRepository<Book>>();
            IList<Book> list = new List<Book>();
            list.Add(new Book(2, "Hiba revut' voly", "Panas Myrnyi"));
            list.Add(new Book(3, "Natalka Poltavka", "Ivan Kotlyarevsky"));
            mockrepo.Setup(x => x.GetBooks()).Returns(list);
            mockrepo.Setup(x => x.GetBook(1)).Returns(new Book(1, "Tvory", "Lina Kostenko"));
            mockrepo.Setup(x => x.AddBook(new Book("Tvory", "Lina Kostenko"))).Returns(new Book(1, "Tvory", "Lina Kostenko"));
            mockrepo.Setup(x => x.UpdateBook(It.IsAny<Book>())).Returns(new Book(3, "Eneida", "Ivan Kotlyarevsky"));
            mockrepo.Setup(x => x.DeleteBook(4)).Returns(new Book(4, "Zemlya", "Olha Kobylyanska"));
            controller = new BooksController(mockrepo.Object);
        }
        
        //The tests are designed (according to the Nnit) to run in alphabetical order of its names
        [Test]
        public void Mock_GetBooks()
        {
            IConvertToActionResult res=controller.GetBooks();
            IActionResult response=res.Convert();
            OkObjectResult result = response as OkObjectResult;
            Assert.AreEqual(200, result.StatusCode);
            IList<Book> actual = result.Value as List<Book>;
            Assert.AreEqual("Panas Myrnyi", actual[0].Author);
        }

        [Test]
        public void Mock_GetBook()
        {
            IConvertToActionResult res=controller.GetBook(1);
            IActionResult response=res.Convert();
            OkObjectResult result = response as OkObjectResult;
            Assert.AreEqual(200, result.StatusCode);
            Book actual = result.Value as Book;
            Assert.AreEqual("Lina Kostenko", actual.Author);
        }

        [Test]
        public void Mock_AddBook()
        {
            IConvertToActionResult res=controller.PostBook(new Book("Tvory", "Lina Kostenko"));
            IActionResult response=res.Convert();
            CreatedAtActionResult result = response as CreatedAtActionResult;
            Assert.AreEqual(201, result.StatusCode);
            Book actual = result.Value as Book;
            Assert.AreEqual("Tvory", actual.Title);
        }

        [Test]
        public void Mock_UpdateBook()
        {
            IActionResult response = controller.PutBook(3,new Book(3, "Eneida", "Ivan Kotlyarevsky"));
            NoContentResult result = response as NoContentResult;
            Assert.AreEqual(204, result.StatusCode);
        }

        [Test]
        public void Mock_DeleteBook()
        {
            IActionResult response=controller.DeleteBook(4);
            NoContentResult result = response as NoContentResult;
            Assert.AreEqual(204, result.StatusCode);
        }

  }


}