using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;

using BooksApi.Controllers;
using BooksApi.Models;
using BooksApi.Repositories;

using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using NUnit.Framework;
using Newtonsoft.Json;

namespace BooksApi.Tests
{
    [TestFixture]
    class ControllerTest
    {
        BookRepository repo;
        BooksController controller;

        [OneTimeSetUp]
        public void Setup()
        {
            repo = new BookRepository();
            controller = new BooksController(repo);
        }

        //The tests are designed (according to the Nnit version) to run in alphabetical order of its names
        [Test]
        public void  Test_AddBook()
        {
            IConvertToActionResult res1=controller.PostBook(new Book("Praporonosci", "Oles' Honchar"));
            IActionResult response=res1.Convert();
            Assert.NotNull(response);
            CreatedAtActionResult result = response as CreatedAtActionResult;
            Assert.NotNull(result);
            Console.Write(result.Value);
            Assert.AreEqual(201, result.StatusCode);
            Book actual = result.Value as Book;
            Assert.AreEqual("Praporonosci", actual.Title);
            
            IActionResult response2 = ((IConvertToActionResult)controller.PostBook(new Book("Koni ne vynni", "Mykhaylo Kotyubynskyy"))).Convert();
            Assert.NotNull(response2);
            CreatedAtActionResult result2 = response2 as CreatedAtActionResult;
            Assert.NotNull(result2);
            Assert.AreEqual(201, result2.StatusCode);
            Book actual2 = result2.Value as Book;
            Assert.AreEqual("Koni ne vynni", actual2.Title);
        }

        [Test]
        public void Test_GetAllBook()
        {
            IConvertToActionResult res=controller.GetBooks();
            IActionResult response=res.Convert();
            Assert.NotNull(response);
            OkObjectResult result = response as OkObjectResult;
            Assert.NotNull(result);
            Assert.AreEqual(200, result.StatusCode);
            IList<Book> books = result.Value as List<Book>;

            Assert.AreEqual(1, books.Count);
            Assert.AreEqual(books[0].Author, "Oles' Honchar");
            
        }
        
        [Test]
        public void Test_GetBook()
        {
            IConvertToActionResult res=controller.GetBook(1);
            IActionResult response=res.Convert();
            Assert.NotNull(response);
            OkObjectResult result = response as OkObjectResult;
            Assert.NotNull(result);
            Assert.AreEqual(200, result.StatusCode);
            Book book = result.Value as Book;
            Assert.AreEqual(book.Title, "Praporonosci");
        } 

        [Test]
        public void Test_UpdateBook()
        {
            IActionResult response=controller.PutBook(1,new Book(1, "Sobor", "Oles' Honchar"));
            Assert.NotNull(response);
            NoContentResult result = response as NoContentResult;
            Assert.NotNull(result);
            Assert.AreEqual(204, result.StatusCode);
            
            
            IConvertToActionResult res=controller.GetBook(1);
            IActionResult response2=res.Convert();
            Assert.NotNull(response2);
            OkObjectResult result2 = response2 as OkObjectResult;
            Book book = result2.Value as Book;
            Assert.AreEqual(book.Title, "Sobor");
        }


        [Test]
        public void Test_DeleteBook()
        {
            IActionResult response=controller.DeleteBook(2);
            Assert.NotNull(response);
            NoContentResult result = response as NoContentResult;
            Assert.NotNull(result);
            Assert.AreEqual(204, result.StatusCode);
        }

        [OneTimeTearDown]
        public void close()
        {
          
        }
         
    }
}