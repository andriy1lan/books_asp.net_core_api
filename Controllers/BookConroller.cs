using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Diagnostics;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Mvc;

using BooksApi.Models;
using BooksApi.Repositories;
using Microsoft.Extensions.DependencyInjection;

namespace BooksApi.Controllers
{
  //  [Produces("application/json")] 
    [Route("api/[controller]")]
    [ApiController]
    public class BooksController : ControllerBase
    {

        IRepository<Book> repo;
     // BookRepository repo = new BookRepository();

        /* public BooksController() {
          repo = new BookRepository();
        } */

        [ActivatorUtilitiesConstructor]
        public BooksController(IRepository<Book> repo)
        {
            this.repo = repo;
        }

        // GET api/books
        [HttpGet]
        public ActionResult<IList<Book>> GetBooks()
        {
            IList<Book> books = repo.GetBooks();
            if (books.Count!= 0)
            {
                return Ok(books);
            }
            else { return NotFound(); }
        }

        // GET api/books/5
        [HttpGet("{id}")]
        public ActionResult<Book> GetBook(int id)
        {
            Book book = repo.GetBook(id);
            if (book != null)
            {
                return Ok(book);
            }
            else { return NotFound(); }
        }

       // POST api/books
        [HttpPost]
        public ActionResult<Book> PostBook([FromBody]Book book)
        {
            repo.AddBook(book);
            return CreatedAtAction(nameof(GetBook), new { id = book.Id }, book);
        }

        // PUT api/books/5
        [HttpPut("{id}")]
        public ActionResult PutBook(int id,[FromBody]Book book)
        {
            if (id!= book.Id)
               {
                  return BadRequest();
               }
            Book book0 = repo.UpdateBook(book);
            if (book0 != null)
            {
                return NoContent();
            }
            else { return NotFound(); }
        }

        // DELETE api/books/5
        [HttpDelete("{id}")]
        public ActionResult DeleteBook(int id)
        {
            Book book = repo.DeleteBook(id);
            if (book != null)
            {
                return NoContent();
            }
            else { return NotFound(); }

        }

      [NonAction]
      public void  DisableRepo() {
      repo.ClearRepo();
      }
    }
}
